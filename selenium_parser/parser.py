from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

class Parser(object):

    def __init__(self):

        options = Options()
        # options.headless = True

        self.__browser = webdriver.Firefox(options=options)

    def getUrl(self, url):
        self.__browser.get(url)

    def getElement(self, config_parse):
        if config_parse["type"] == 'css':
            return self.__browser.find_element_by_css_selector(config_parse["value"])
        if config_parse["type"] == 'class':
            return self.__browser.find_element_by_class_name(config_parse["value"])
        if config_parse["type"] == 'id':
            return self.__browser.find_element_by_id(config_parse["value"])
        if config_parse["type"] == 'name':
            return self.__browser.find_element_by_name(config_parse["value"])
        if config_parse["type"] == 'xpath':
            return self.__browser.find_element_by_xpath(config_parse["value"])

    def getElements(self, config_parse):
        if config_parse["type"] == 'css':
            return self.__browser.find_elements_by_css_selector(config_parse["value"])
        if config_parse["type"] == 'class':
            return self.__browser.find_elements_by_class_name(config_parse["value"])
        if config_parse["type"] == 'id':
            return self.__browser.find_elements_by_id(config_parse["value"])
        if config_parse["type"] == 'name':
            return self.__browser.find_elements_by_name(config_parse["value"])
        if config_parse["type"] == 'xpath':
            return self.__browser.find_elements_by_xpath(config_parse["value"])

    def getChildElement(self, element, config_parse):
        if config_parse["type"] == 'css':
            return element.find_element_by_css_selector(config_parse["value"])
        if config_parse["type"] == 'class':
            return element.find_element_by_class_name(config_parse["value"])
        if config_parse["type"] == 'id':
            return element.find_element_by_id(config_parse["value"])
        if config_parse["type"] == 'name':
            return element.find_element_by_name(config_parse["value"])
        if config_parse["type"] == 'xpath':
            return element.find_element_by_xpath(config_parse["value"])

    def getChildElements(self, element, config_parse):
        if config_parse["type"] == 'css':
            return element.find_elements_by_css_selector(config_parse["value"])
        if config_parse["type"] == 'class':
            return element.find_elements_by_class_name(config_parse["value"])
        if config_parse["type"] == 'id':
            return element.find_elements_by_id(config_parse["value"])
        if config_parse["type"] == 'name':
            return element.find_elements_by_name(config_parse["value"])
        if config_parse["type"] == 'xpath':
            return element.find_elements_by_xpath(config_parse["value"])


    def getElementByTimeout(self, config_parse, timeout):
        type = None

        if config_parse["type"] == 'css':
            type = By.CSS_SELECTOR
        elif config_parse["type"] == 'class':
            type = By.CLASS_NAME
        elif config_parse["type"] == 'id':
            type = By.ID
        elif config_parse["type"] == 'name':
            type = By.NAME
        elif config_parse["type"] == 'xpath':
            type = By.XPATH
        else:
            return type

        return WebDriverWait(self.__browser, timeout).until(
            EC.visibility_of_element_located((type, config_parse['value']))
        )

    def getElementsByTimeout(self, config_parse, timeout):

        type = None

        if config_parse["type"] == 'css':
            type = By.CSS_SELECTOR
        elif config_parse["type"] == 'class':
            type = By.CLASS_NAME
        elif config_parse["type"] == 'id':
            type = By.ID
        elif config_parse["type"] == 'name':
            type = By.NAME
        elif config_parse["type"] == 'xpath':
            type = By.XPATH
        else:
            return type

        return WebDriverWait(self.__browser, timeout).until(
            EC.visibility_of_all_elements_located((type, config_parse['value']))
        )

    def insertText(self, element, text):
        return element.send_keys(text)

    def wait(self, second):
        self.__browser.implicitly_wait(second)

    def addCookie(self, cookie_list):
        return self.__browser.add_cookie(cookie_list)

    def getCookie(self):
        all_cookies = self.__browser.get_cookies()
        return all_cookies.items()

    def goBack(self):
        return self.__browser.back()

    def goUp(self):
        return self.__browser.forward()

    def __del__(self):
        self.__browser.quit()
