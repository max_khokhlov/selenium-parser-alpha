from config import config
from core.db import DB
from datetime import datetime


class Controller(object):

    _table = None
    _column = None
    _parent_column = None

    def __init__(self):
        self.db = DB(config)
        self.datetime = datetime

    def get_communication_field(self, fields):
        for key, field in fields.items():
            if key == self._column:
                return field

    def addCreatedDateInsert(self, list):
        for item in list:
            if "created_at" not in item.keys():
                now = self.datetime.now()
                item["created_at"] = now.strftime("%d-%m-%Y %H:%M:%S")

        return list

    @staticmethod
    def filter(item, keys=[]):
        output = {}

        for key, value in item.items():
            if key in keys:
                output[key] = value

        return output

    @staticmethod
    def merge_lists_by_field_name(parent_list=[], object_list={}, field_name="", common_field=""):
        for list in parent_list:
            if object_list[list[common_field]] is not None:
                list[field_name] = object_list[list[common_field]]

        return parent_list

    def insertRows(self, list, table=False, returnColumn="name"):
        return self.db.insert(self._table if table is False else table, list, returnColumn=returnColumn)

    def select(self, where=[], columns=[], all=True, limit=False):
        result = self.db.select(self._table, columns, where, all=all, limit=limit)
        return result

    def find(self, id):
        result = self.select(['id', '=', id], all=False)
        return result

    def key_replacement(self, list, parent_key):
        for item in list:
            if "parent_id" in item:
                item[parent_key] = item.pop('parent_id')

        return list

    def getColumn(self):
        return self._column

    def getTable(self):
        return self._table



