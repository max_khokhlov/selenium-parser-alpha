import codecs
import time
from config import config
from datetime import datetime
import os


class Logger(object):

    file = None
    path = 'storage/log/'
    start = None
    end = None

    def __init__(self):
        date = datetime.today().strftime("_%d_%m_%Y")
        self.path = os.path.normpath(self.path)
        self.file = os.path.join(config['path'], self.path, str(u"log_"+date))

    def log(self, string, start=False, end=False):

        if start is True:
            self.start = time.time()

        date_time = datetime.now().strftime("%d-%m-%Y %H:%M:%S")
        with codecs.open(self.file, 'a+', "utf-8") as file:

            if end is True and self.start is not None:
                interval = time.time() - self.start
                string_interval = u" Время выполнения: " + str(interval) + " сек \r\n"
                print(string_interval)
                file.write(string_interval)

            string_logger = str(u"" + string + " time=" + date_time + "\r\n")
            print(string_logger)
            file.write(string_logger)
            file.close()

    def log_name(self, string, file_name=None, start=False, end=False):

        if file_name is None:
            raise Exception("Not filename")

        if start is True:
            self.start = time.time()

        date = datetime.today().strftime("%d_%m_%Y")
        self.path = os.path.normpath(self.path)
        file = os.path.join(self.path, str(u""+file_name+date))

        date_time = datetime.now().strftime("%d-%m-%Y %H:%M:%S")

        with codecs.open(file, "a+", "utf-8") as custom_log:

            if end is True and self.start is not None:
                interval = time.time() - self.start
                string_interval = u" Время выполнения: " + str(interval) + " сек \r\n"
                print(string_interval)
                file.write(string_interval)

            string_logger = str(u"" + string + " time=" + date_time + "\r\n")
            print(string_logger)
            custom_log.write(string_logger)
            custom_log.close()

