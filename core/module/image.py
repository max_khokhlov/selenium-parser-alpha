import urllib.request
from urllib.parse import urlparse, urlsplit
import os
import re
from datetime import datetime
from config import config

class Image(object):

    path = 'static/images'

    def __init__(self, path):
        self.dbpath = os.path.normpath(os.path.join(self.path, path))
        self.path = os.path.normpath(os.path.join(config["path"], self.path, path))
        if os.path.exists(self.path) is False:
            self.createDir()

    def normPath(self, path1, path2):
        paths = os.path.join(path1, path2)
        return re.sub(r"\\", "/", paths)

    def createDir(self):
        try:
            os.makedirs(self.path, mode=0o755)
        except:
            print("Error create dir")

    def download(self, url):

        o = urlparse(url)
        url = o.scheme + "://" + o.netloc + o.path

        name = os.path.split(url)

        if len(name[1]) is 0:
            return False

        name_file = str(name[1])

        if os.path.exists(os.path.join(self.path, name_file)) is True:
            return self.normPath(self.dbpath, name_file)


        image_url = urllib.request.urlopen(url).read()

        with open(os.path.join(self.path, name_file), 'wb') as image:
            image.write(image_url)
            image.close()
            return self.normPath(self.dbpath, name_file)
