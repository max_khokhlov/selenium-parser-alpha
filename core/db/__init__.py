import psycopg2
from psycopg2 import sql


class DB(object):
    connect = None

    def __init__(self, config):
        try:
            self.connect = psycopg2.connect("dbname='%s' user='%s' password='%s' host='%s'" % (config["database"], config["user"], config["password"], config["host"]))
            self.__select_obj = None
        except Exception as e:
            print(e.message)

    @staticmethod
    def extend(config):
        if DB.connect is None:
            return DB(config)
        else:
            return DB.connect

    def select(self, table, columns, where, all=False, limit=False) -> list:
        with self.connect.cursor() as cursor:
            column_str = ",".join(columns) if len(columns) > 0 else "*"

            if len(where) == 0:
                cursor.execute("SELECT %s FROM %s " % (column_str, table))
            elif isinstance(where[0], str, int):
                cursor.execute("SELECT %s FROM %s WHERE %s" % (column_str, table, "".join(where)))
            else:
                cursor.execute("SELECT %s FROM %s WHERE %s" % (column_str, table, self.getWhere(where)))

            if limit is not False:
                return cursor.fetchmany(limit)

            if all is True:
                return cursor.fetchall()
            else:
                return cursor.fetchone()

    def query(self, query) -> object:
        with self.connect.cursor() as cursor:
            cursor.execute(query)
            return cursor

    def getWhere(self, array) -> str:
        return " and ".join(["".join(val) for val in array if len(val) == 3])

    def getStringField(self, field) -> str:
        return "%(" + field + ")s"

    def insert(self, table, list_val, returnColumn="name") -> object:

        with self.connect.cursor() as cursor:
            self.connect.autocommit = True

            fields = list_val[0].keys()

            sql = "INSERT INTO %s (%s) VALUES (%s) ON CONFLICT (%s) DO UPDATE SET updated_at=CURRENT_TIMESTAMP RETURNING id, %s" % (table, str(",".join(fields)), ','.join(map(self.getStringField, fields)), returnColumn, returnColumn)

            returnArr = []

            for item in list_val:
                cursor.execute(sql, item)
                returnArr.append(cursor.fetchone())

            return returnArr

