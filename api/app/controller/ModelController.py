from core.controller import Controller


class ModelController(Controller):

    _table = 'model'
    _table_image = 'model_image'
    _table_count = 'model_count_ads'
    _table_url = 'model_url'
    _column = 'name'
    _parent_column = 'brand_id'
    _id_column = 'model_id'

    def insert(self, list):

        list = self.key_replacement(list, self._parent_column)

        result_id = self.insertModels(list)

        list = self.merge_lists_by_field_name(parent_list=list, object_list=result_id, field_name=self._id_column, common_field='name')

        self.insertCount(list)
        self.insertUrl(list)

    def insertModels(self, list):

        keys_table = [
            self._parent_column,
            "name"
        ]

        insertList = [self.filter(item, keys=keys_table) for item in list]
        return_rows = self.insertToTable(insertList, return_column='name')
        return dict((crt[1], crt[0]) for crt in return_rows)

    def insertCount(self, list):

        keys_table = [
            self._id_column,
            "count"
        ]

        insertList = [self.filter(item, keys=keys_table) for item in list]
        return_rows = self.insertToTable(insertList, table=self._table_count, return_column='model_id')
        return dict((crt[1], crt[0]) for crt in return_rows)

    def insertUrl(self, list):

        keys_table = [
            self._id_column,
            "url_model",
        ]

        insertList = [self.filter(item, keys=keys_table) for item in list]
        return_rows = self.insertToTable(insertList, self._table_url, return_column='url_model')

        return return_rows

    def insertImage(self, list, returnColumn="url"):
        list = self.addCreatedDateInsert(list)
        return self.insertRows(list, self._table_image, returnColumn=returnColumn)

    def getBrandByColumn(self, value):
        result = self.select([self._column, "=", value])
        return result

    def insertToTable(self, list, table=False, return_column='name'):
        list = self.addCreatedDateInsert(list)
        if table is False:
            return self.insertRows(list, returnColumn=return_column)
        else:
            return self.insertRows(list, table, returnColumn=return_column)



