from core.controller import Controller


class BrandController(Controller):

    _table = 'brand'
    _table_image = 'brand_image'
    _table_count = 'brand_count_ads'
    _table_url = 'brand_url'
    _column = 'name'

    def insert(self, list):
        result_id = self.insertBrand(list)

        list = self.merge_lists_by_field_name(parent_list=list, object_list=result_id, field_name='brand_id', common_field='name')

        self.insertCount(list)
        self.insertUrl(list)

        return result_id

    def insertBrand(self, list):

        keys_table = [
            "name"
        ]

        insertList = [self.filter(item, keys=keys_table) for item in list]
        return_rows = self.insertToTable(insertList, return_column='name')

        return dict((crt[1], crt[0]) for crt in return_rows)

    def insertCount(self, list):

        keys_table = [
            "count",
            "brand_id"
        ]

        insertList = [self.filter(item, keys=keys_table) for item in list]
        return_rows = self.insertToTable(insertList, self._table_count, return_column='brand_id')

        return return_rows

    def insertUrl(self, list):

        keys_table = [
            "url_brand",
            "brand_id"
        ]

        insertList = [self.filter(item, keys=keys_table) for item in list]
        return_rows = self.insertToTable(insertList, self._table_url, return_column='url_brand')

        return return_rows

    def insertToTable(self, list, table=False, return_column='name'):
        list = self.addCreatedDateInsert(list)
        if table is False:
            return self.insertRows(list, returnColumn=return_column)
        else:
            return self.insertRows(list, table, returnColumn=return_column)

    def insertImage(self, list, returnColumn="url"):
        list = self.addCreatedDateInsert(list)
        return self.insertRows(list, self._table_image, returnColumn=returnColumn)

    def getBrandByColumn(self, value):
        result = self.select([self._column, "=", value])
        return result



