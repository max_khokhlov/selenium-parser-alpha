import codecs, json
# CustomModules
import time
from core.module import image
from selenium_parser import parser
from core.module.logger import Logger


class Parser(object):
    prs = None
    config_actions = None

    def __init__(self, config_parser_file):
        self.__config_file = config_parser_file
        self.__logger = Logger()
        self.getConfig()

    def getConfig(self):
        with codecs.open(self.__config_file, 'r', "utf8") as file:
            self.config_actions = json.loads(file.read())

    def create(self):
        url_config = self.config_actions["parent_url"]
        self.prs = parser.Parser()
        self.__logger.log("Запущен парсер")
        self.prs.getUrl(url_config)

    def start_action(self, action):
        self.__logger.log("Шаг '" + action["name_actions"]+"' стартовал", start=True)

    def end_action(self, action):
        self.__logger.log("Шаг '" + action["name_actions"]+"' выполнен", end=True)

    def parse(self):

        actions = self.config_actions["actions"]

        for key, action in enumerate(actions):

            if action["group"]:
                self.parse_elements(action)
            else:
                self.parse_element(action)

    def click_elem(self, action_click):

        if action_click is False:
            return False

        elem = self.prs.getElement(action_click)

        elem.click()
        return True

    @staticmethod
    def get_controller(controller):

        if controller is False:
            return False

        module = __import__("api.app.controller.%s" % (controller), fromlist=['object'])
        classObj = getattr(module, controller)

        return classObj()

    def parse_elements(self, action, parent_id=False):

        self.start_action(action)

        controller = self.get_controller(action['controller'])

        # elem_value = self.prs.getElement(action)

        self.click_elem(action['click']['pre_click'])

        if controller is not False and len(action['fields']) > 0:

            if action['timeout'] is not False:
                elements = self.prs.getElementsByTimeout(action, 30)
            else:
                elements = self.prs.getElements(action)

            element_arrs = []

            for element in elements:

                if parent_id:
                    obj_field = {
                        "parent_id": parent_id
                    }
                else:
                    obj_field = {}

                for key, field in action['fields'].items():
                    if field['value'] is False and field['type'] is False:
                        obj_field[key] = element.get_attribute(field['attribute'])
                    else:
                        child_elem = self.prs.getChildElement(element, field)
                        obj_field[key] = child_elem.get_attribute(field['attribute'])

                element_arrs.append(obj_field)

            return_rows = controller.insert(element_arrs)

            if len(action["childrenActions"]) > 0:
                self.parse_child_action(action, action['childrenActions'], return_rows)

                self.click_elem(action['click']['post_click'])

        self.end_action(action)

    def parse_child_action(self, parent_action, actions, parent, index=0):

        if index > 0:
            self.click_elem(parent_action['click']['pre_click'])

        elements = self.prs.getElements(parent_action)

        if index not in [key for key, _ in enumerate(elements)]:
            return False

        controller = self.get_controller(parent_action['controller'])

        field = controller.get_communication_field(parent_action['fields'])

        if field['value'] is False and field['type'] is False:
            field_element = elements[index].get_attribute(field['attribute'])
        else:
            child_elem = self.prs.getChildElement(elements[index], field)
            field_element = child_elem.get_attribute(field['attribute'])

        elements[index].click()

        parent_id = parent[field_element]

        for action in actions:
            if action["group"]:
                self.parse_elements(action, parent_id=parent_id)
            else:
                self.parse_element(action, parent_id=parent_id)

        self.prs.goBack()

        index = int(index) + 1
        self.parse_child_action(parent_action, actions, parent, index=index)

    def parse_element(self, action, parent_id=False):

        self.start_action(action)

        controller = self.get_controller(action['controller'])

        # elem = self.prs.getElement(action)

        self.click_elem(action['click']['pre_click'])

        if controller is not False and len(action['fields']) > 0:

            if action['timeout']:
                element = self.prs.getElementByTimeout(action, 30)
            else:
                element = self.prs.getElement(action)

            element_arrs = []

            if parent_id:
                obj_field = {
                    "parent_id": parent_id
                 }
            else:
                obj_field = {}

            for key, field in action['fields'].items():
                if field['value'] is False and field['type'] is False:
                    obj_field[key] = element.get_attribute(field['attribute'])
                else:
                    child_elem = self.prs.getChildElement(element, field)
                    obj_field[key] = child_elem.get_attribute(field['attribute'])

                element_arrs.append(obj_field)

            return_rows = controller.insert(element_arrs)

            if len(action["childrenActions"]) > 0:
                self.parse_child_action(action, action['childrenActions'], return_rows)

        self.click_elem(action['click']['post_click'])

        self.end_action(action)


parser_class = Parser("./test_config_parser/auto_ru.json")
parser_class.create()
parser_class.parse()