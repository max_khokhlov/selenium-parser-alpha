import codecs, json, re, sys

# CustomModules
from config import config
from core.db import DB
from core.module import image
import api.app.controller as ctrl
from selenium_parser import parser


class BaseParser(object):
    prs = None
    config_actions = None
    imageController = None

    def __init__(self, config_parser_file):
        self.__config_file = config_parser_file
        self.getConfig()

    def getConfig(self):
        with codecs.open(parser_config_file, 'r', "utf8") as file:
            self.config_actions = json.loads(file.read())

    def create(self):
        url_config = self.config_actions["parent_url"]
        self.prs = parser.Parser()
        self.prs.getUrl(url_config)

    def saveElements(self, elems, action, parent_id=False):

        controllerClass = self.getController(action["controller"])
        rowsArr = []

        for elem in elems:
            text = elem.get_attribute('text')

            if parent_id is False:
                rowInsert = dict.fromkeys([action["column"]], text)
            else:
                rowInsert = {
                    action["column"]: text,
                    action["parent_column"]: parent_id
                }

            rowsArr.append(rowInsert)

        returnRows = controllerClass.insert(rowsArr, returnColumn=action["column"])
        # crt[0] = id
        # crt[1] = config column
        parent = dict((crt[1], crt[0]) for crt in returnRows)

        if action["img"] is not False:
            self.imageController = image.Image(action["img"]["path"])
            self.saveImage(elems, parent, action["img"], controller=controllerClass)

        if len(action["childrenActions"]) > 0 and action['click'] is True:
                self.saveChildElements(action, action["childrenActions"], parent)


    def saveImage(self, elems, parentRows, action, controller=None):

        images = self.prs.getElements(action)

        if len(images) == 0:
            return False

        rowsInsert = []

        for key, elem in enumerate(elems):
            text = elem.get_attribute('text')
            id = parentRows[text]

            if key in [key for key, _ in enumerate(images)]:
                image = images[key].get_attribute('src')
                local_url = self.imageController.download(image)

                if local_url is False:
                    continue

                rowInsert = {
                    action["column"]: local_url,
                    action["parent_column"]: id
                }
                rowsInsert.append(rowInsert)

        controller.insertImage(rowsInsert, returnColumn=action["column"])

    def saveChildElements(self, parent_action, actions, parent, index=0):

        elems = self.prs.getElements(parent_action)

        if index not in [key for key, _ in enumerate(elems)]:
            return False

        text = elems[index].get_attribute('text')

        elems[index].click()

        parent_id = parent[text]

        for action in actions:
            if action["group"]:
                сhildren_elems = self.prs.getElements(action)
                self.saveElements(сhildren_elems, action, parent_id=parent_id)
            else:
                сhildren_elem = self.prs.getElement(action)
                self.saveElement(сhildren_elem, action, parent_id=parent_id)

        self.prs.goBack()

        index = int(index) + 1
        self.saveChildElements(parent_action, actions, parent, index=index)


    def getController(self, controller):
        module = __import__("api.app.controller.%s" % (controller), fromlist=['object'])
        classObj = getattr(module, controller)
        return classObj()

    def saveElement(self, elem, action):
        with codecs.open('test.html', 'a', "utf8") as file:
            file.write(elem.get_attribute('innerHTML'))
            print('end')

    def parse(self):

        for action in self.config_actions["actions"]:

            if action["group"]:
                elems = self.prs.getElements(action)
                self.saveElements(elems, action)
            else:
                elem = self.prs.getElement(action)
                self.saveElement(elem, action)


parser_config_file = './test_config_parser/auto_mail.json'
base = BaseParser(parser_config_file)
base.create()
base.parse()
